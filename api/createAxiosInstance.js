import axios from "axios";

const createAxiosInstance = ({
  baseURL,
  config = {},
  defaultErrorHandler = null,
  refreshOnCORSError = true,
}) => {
  const restplusInstance = axios.create({
    baseURL,
    ...config,
  });

  restplusInstance.interceptors.request.use(function (config) {
    config.withCredentials = true;
    return config;
  });

  restplusInstance.interceptors.response.use(
    (res) => res,
    (err) => {
      // If the users is not recognized by the sso refresh the page
      // so that it redirects to the sso page
      if (typeof err.response === "undefined" || err.response.status === 302) {
        if (
          process.env.NODE_ENV !== "production" &&
          process.env.NODE_ENV !== "development"
        ) {
          // In non-prod setups the SPA and API can end up on different origins and behind different SSO.
          // It better never happens in production.
          console.info(err.request);
          alert(
            "A network error occurred, possibly related to CORS. " +
              "Since this is not a production instance, the requested URL will be opened in a new window, allowing the browser to collect the cookies it needs."
          );
          var popup = window.open(process.env.VUE_APP_API_URL, "cookieWindow");
          window.setInterval(function () {
            if (popup !== null && popup.closed) {
              location.reload();
            }
          }, 500);
        } else {
          // We can't seem to catch the 302 status code as an error,
          // however, since it redirects to another domain it causes
          // a CORS error which makes error.response be undefined here.  This assumes that any time
          // error.response is undefined that we need to refresh to redirect to the login page
          if (refreshOnCORSError) {
            location.reload();
          }
        }
      }

      if (defaultErrorHandler) {
        defaultErrorHandler(err);
      }

      throw err;
    }
  );

  return {
    get: (url, params) =>
      restplusInstance.get(url, { params }).then((result) => result.data),
    post: (...params) =>
      restplusInstance.post(...params).then((result) => result.data),
    put: (...params) =>
      restplusInstance.put(...params).then((result) => result.data),
    delete: (url, params) =>
      restplusInstance
        .delete(url, { data: params })
        .then((result) => result.data),
    upload: (url, formData) =>
      restplusInstance.post(url, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }),
  };
};
export default createAxiosInstance;
